<?php
namespace App\project\db;

use PDO;
class DB {
    public $pdo;
    const host = 'localhost';
    public function __construct(){
        try{
            $this->pdo = new PDO('mysql:host=localhost;dbname=bitm_project', 'root', '');
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch (PDOException $e){
            echo "Error: ". $e->getMessage();
        }
    }
}