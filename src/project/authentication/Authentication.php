<?php
namespace App\project\authentication;

require_once("../../../vendor/autoload.php");
use App\project\db\DB;
class Authentication extends DB {
    private $userName;
    private $firstName;
    private $lastName;
    private $email;
    private $password;

    public function __construct(){
        parent::__construct();
        session_start();
    }
    public function setData($data=''){
        if(array_key_exists('userName', $data)){
            $this->userName = $data['userName'];
        }if(array_key_exists('firstName', $data)){
            $this->firstName = $data['firstName'];
        }if(array_key_exists('lastName', $data)){
            $this->lastName = $data['lastName'];
        }if(array_key_exists('password', $data)){
            $this->password = $data['password'];
        }if(array_key_exists('email', $data)){
            $this->email = $data['email'];
        }
        return $this;
    }
    public function register(){
        try{
            $query = "INSERT INTO `auth` (`unique_id`,`user_name`,`first_name`,`last_name`,`password`,`email`)
                  VALUES (:unique,:uName,:fName,:lName,:pass,:mail)";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':unique' => uniqid(),
                    ':uName' => $this->userName,
                    ':fName' => $this->firstName,
                    ':lName' => $this->lastName,
                    ':pass' => $this->password,
                    ':mail' => $this->email,
                )
            );
            if($stmt){
                $_SESSION['msg'] = "Registration Success";
                header('location: ../login/create.php');
            }
        }catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function login()
    {
        try{
            $query = "SELECT * FROM `auth` WHERE `user_name`=:uName AND `password`=:pass";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':uName' => $this->userName,
                    ':pass' => $this->password,
                )
            );
            //return count($stmt->fetchAll());
            return $stmt->fetchAll();
        }catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}


